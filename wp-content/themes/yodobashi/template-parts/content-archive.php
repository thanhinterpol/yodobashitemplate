<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dichvuweb
 */

?>

<div class="col-md-3 col-sm-6 col-xs-12">
    <article class="camera">
        <a href="<?php the_permalink(); ?>">
            <p class="img"><img src="<?php the_field('large_thumbnail') ?>"></p>
            <h3><?php the_title() ?></h3>
        </a>
        <p><?php echo mb_strimwidth(get_the_excerpt(), 0, 115, '...'); ?></p>
    </article>
</div>
