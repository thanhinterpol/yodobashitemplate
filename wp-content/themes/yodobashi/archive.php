<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package dichvuweb
 */

get_header();
$category = get_category( get_query_var( 'cat' ) );
$cat_id = $category->cat_ID;
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <div class="page-head">
                            <img src="<?php echo get_field('category_image','category_'.$cat_id) ?>" alt="" id="main-img">
                            <h1>
                                <?php if(get_field('custom_title','category_'.$cat_id)){
                                    echo get_field('custom_title','category_'.$cat_id);
                                }else{
                                    echo single_cat_title();
                                } ?>
                            </h1>
                        </div>

                        <p class="note">
                            <?php echo category_description( $cat_id ); ?>
                        </p>

                    </div>
                </div><!-- .row -->

                <div class="row">
                    <section>

                        <div class="col-md-12">
                            <h2>新着情報</h2>
                            <p class="sub-note">最新の実写レビューを並べました。期待の新製品や、少しお待たせしてしまったレンズ作例など、更新順にご覧いただけます。どうぞお見逃しなく。</p>
                        </div>
                        <?php
                        $args = array(
                            'posts_per_page'    => 8,
                            'post_type' => 'post'
                        );
                        $the_query = new WP_Query( $args ); ?>

                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php get_template_part( 'template-parts/content', 'archive' ); ?>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                        <?php endif; ?>
                    </section>
                </div><!-- .row -->
                <div class="row">
                    <section>

                        <div class="col-md-12">
                            <h2>新着情報</h2>
                            <p class="sub-note">最新の実写レビューを並べました。期待の新製品や、少しお待たせしてしまったレンズ作例など、更新順にご覧いただけます。どうぞお見逃しなく。</p>
                        </div>
                        <?php
                        $args = array(
                            'posts_per_page'    => 8,
                            'post_type' => 'post'
                        );
                        $the_query = new WP_Query( $args ); ?>

                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php get_template_part( 'template-parts/content', 'archive' ); ?>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                        <?php endif; ?>
                    </section>
                </div><!-- .row -->
                <div class="row">
                    <section>

                        <div class="col-md-12">
                            <h2>新着情報</h2>
                            <p class="sub-note">最新の実写レビューを並べました。期待の新製品や、少しお待たせしてしまったレンズ作例など、更新順にご覧いただけます。どうぞお見逃しなく。</p>
                        </div>
                        <?php
                        $args = array(
                            'posts_per_page'    => 8,
                            'post_type' => 'post'
                        );
                        $the_query = new WP_Query( $args ); ?>

                        <?php if ( $the_query->have_posts() ) : ?>
                            <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                                <?php get_template_part( 'template-parts/content', 'archive' ); ?>
                            <?php endwhile; ?>
                            <?php wp_reset_postdata(); ?>

                        <?php else : ?>
                        <?php endif; ?>
                    </section>
                </div><!-- .row -->
            </div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();
