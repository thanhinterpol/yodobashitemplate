<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dichvuweb
 */

?>

</div><!-- .container -->
<?php
    if(is_home() || is_front_page()){

    }else{
        ?>
        <footer class="ft-pagetop">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="top"><strong><a href="#">Canon（キヤノン） 実写レビュー | Back to top
                                    ▲</a></strong></p>
                    </div>
                </div>
            </div>
        </footer>

        <footer class="ft-navi">
            <div class="container">
                <div class="row">
                    <div class="col-sm-3">
                        <nav>
                            <h4 class="site-title">
                                <a href="<?php echo site_url() ?>">PHOTO YODOBASHI</a><br>
                                <span class="sub-title">ヨドバシカメラ公式オンライン写真マガジン</span>
                            </h4>
                            <?php dynamic_sidebar('footer-1'); ?>
                        </nav>
                    </div>
                    <div class="col-sm-3">
                        <nav>
                            <?php dynamic_sidebar('footer-2'); ?>
                        </nav>
                    </div>
                    <div class="col-sm-3">
                        <nav>
                            <?php dynamic_sidebar('footer-3'); ?>
                        </nav>
                    </div>
                    <div class="col-sm-3">
                        <nav>
                            <?php dynamic_sidebar('footer-4'); ?>
                        </nav>
                    </div>
                </div>
            </div>
        </footer>

        <footer class="ft-bottom">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 bottom-logo">
                        <a href="#">
                            <img src="<?php echo get_template_directory_uri(); ?>/images/footer_yodobashi_logo.gif" alt="ヨドバシ・ドット・コム">
                        </a>
                        <span class="bottom-link">
					<a href="#">● 特定商取引法に基づく表示</a>　<br
                                    class="visible-xs">
					<a href="#">● お買い物方法の詳細・お問い合わせ先</a>
				</span>

                    </div>
                    <div class="col-md-4 copyright">
                        <p>Copyright © Yodobashi Camera Co., Ltd.</p>
                    </div>
                </div>
            </div>
        </footer>
        <?php
    }
?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
