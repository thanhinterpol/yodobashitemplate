$(document).ready(function(){
	var pageTitle = $('title').text();
	var pageUrl = window.location;
	var hashTag = "フォトヨドバシ";

	$('#snsButton').append($('<span />').attr('style', 'display: inline-block; font-size: 12px; line-height: 36px; vertical-align: middle;').text('シェアする'));
  var twSpan = $('<span />').attr('style', 'display: inline-block; margin-left: 10px; vertical-align: middle;');
	twSpan.append(
		$('<a />')
		.attr('title', 'Twitterでリンクを共有する')
		.attr('target', '_blank')
		.attr('style', 'display: block; height: 36px;')
		.attr('href', 'https://twitter.com/share?url='+encodeURIComponent(pageUrl)+'&text='+encodeURIComponent(pageTitle)+'&hashtags='+encodeURIComponent(hashTag))
		.attr('data-hashtags', 'a,b,c')
		.append(
			$('<img />')
			.attr('alt', 'Twitterでリンクを共有する')
			.attr('height', '36')
			.attr('width', '36')
			.attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_twitter_72.png')
			.attr('style', 'margin-bottom: 0; width: auto!important;')
			.mouseout(function(){$(this).attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_twitter_72.png')})
			.mouseover(function(){$(this).attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_twitter_72_hover.png')})
		)
	);
	$('#snsButton').append(twSpan);
	var fbSpan = $('<span />').attr('style', 'display: inline-block; margin-left: 10px; vertical-align: middle;');
	fbSpan.append(
		$('<a />')
		.attr('title', 'Facebookでシェア')
		.attr('target', '_blank')
		.attr('style', 'display: block; height: 36px;')
		.attr('href', 'https://www.facebook.com/sharer/sharer.php?u='+encodeURIComponent(pageUrl))
		.append(
			$('<img />')
			.attr('alt', 'Facebookでシェア')
			.attr('height', '36')
			.attr('width', '36')
			.attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_facebook_72.png')
			.attr('style', 'margin-bottom: 0; width: auto!important;')
			.mouseout(function(){$(this).attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_facebook_72.png')})
			.mouseover(function(){$(this).attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_facebook_72_hover.png')})
		)
	);
	$('#snsButton').append(fbSpan);
	var lineSpan = $('<span />').attr('style', 'display: inline-block; margin-left: 10px; vertical-align: middle;');
	lineSpan.append(
		$('<a />')
		.attr('title', 'LINEで送る')
		.attr('target', '_blank')
		.attr('style', 'display: block; height: 36px;')
		.attr('href', 'https://social-plugins.line.me/lineit/share?url='+encodeURIComponent(pageUrl))
		.append(
			$('<img />')
			.attr('alt', 'LINEで送る')
			.attr('height', '36')
			.attr('width', '36')
			.attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_line_72.png')
			.attr('style', 'margin-bottom: 0; width: auto!important;')
			.mouseout(function(){$(this).attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_line_72.png')})
			.mouseover(function(){$(this).attr('src', 'https://image.yodobashi.com/catalog/20180816-01/common/img/icons/icon_sns_line_72_hover.png')})
		)
	);
	$('#snsButton').append(lineSpan);
});
