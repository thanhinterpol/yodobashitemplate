/*
 * #3977
 */
$(function(){
	var skus=""; var skuAry=new Array(); var skuAryIdx=0;
	var pnmx=""; var pnmxKey="pnmx-";
	var pid=""; var pidKey="pid-";
	var elems=$("*").get();
	for (var i=0;i<elems.length; i++){
		var nm=elems[i].getAttribute('class')||elems[i].getAttribute('className');
		if (nm!=null) {
			if(pnmx==""&&nm.indexOf(pnmxKey)!=-1){
				pnmx="&pnmx="+nm.substring(nm.indexOf(pnmxKey)+pnmxKey.length,nm.length);
			}
			if(pid==""&&nm.indexOf(pidKey)!=-1){
				pid="&pid="+nm.substring(nm.indexOf(pidKey)+pidKey.length,nm.length);
			}
			nm=""+nm.match(/[0-9]{18}/);
			if(nm.length==18&&skus.indexOf(nm)<0){
				skus=skus.concat("&sku="+nm);
				skuAry[skuAryIdx]=nm; skuAryIdx++;
			}
		}
	}
	if(skus!='') {
		var retUrl=encodeURIComponent(location.href);
		$.ajax({
			//
			"url":"http://www.yodobashi.com/ws/api/ec/products-rashiku?returnUrl="+retUrl+skus+pnmx+pid+"&callback=?",
			"dataType": "jsonp",
			"success": function(data) {
				var jsData = eval(data);
				for (var i = 0; i < jsData.item.length; i++) {
					var curItem = jsData.item[i];
					var curProductUrl = curItem.productUrl.replace(/index.html/g, "");
					var curProductUrlRef = curItem.productUrlRef.replace(/index.html/g, "");
					var curParent = ".Prd-" + curItem.sku;
					$(curParent).each(function(){
						if(curItem.cartImageTag == '') {
							$(this).html('<div class="pubEnd"></div>');
						}else{
							$(curParent + " .Prd-productName").each(function(){
								$(this).html(curItem.productName);
							});
							$(curParent + " .Prd-brandName").each(function(){
								$(this).html(curItem.brandName);
							});
							$(curParent + " .Prd-productSummary").each(function(){
								$(this).html(curItem.productSummary);
							});
							$(curParent + " .Prd-scheduledMessage").each(function(){
								$(this).html(curItem.scheduledMessage);
							});
							$(curParent + " .Prd-productUrl").each(function(){
								$(this).html(curProductUrl);
							});
							$(curParent + " .Prd-productUrlRef").each(function(){
								$(this).attr("href", curProductUrlRef);
							});
							$(curParent + " .Prd-salesPrice").each(function(){
								$(this).html(curItem.salesPrice);
							});
							$(curParent + " .Prd-fixedPrice").each(function(){
								$(this).html(curItem.fixedPrice);
							});
							$(curParent + " .Prd-discountRate").each(function(){
								$(this).html(curItem.discountRate);
							});
							$(curParent + " .Prd-point").each(function(){
								$(this).html(curItem.point);
							});
							$(curParent + " .Prd-pointRate").each(function(){
								$(this).html(curItem.pointRate);
							});
							$(curParent + " .Prd-salesReleaseDate").each(function(){
								$(this).html(curItem.salesReleaseDate);
							});
							$(curParent + " .Prd-productNotesTitle").each(function(){
								$(this).html(curItem.productNotesTitle);
							});
							$(curParent + " .Prd-productNotesText").each(function(){
								$(this).html(curItem.productNotesText);
							});
							$(curParent + " .Prd-productNotesUrl").each(function(){
								$(this).html(curItem.productNotesUrl);
							});
							$(curParent + " .Prd-productNotesUrlTag").each(function(){
								$(this).html(curItem.productNotesUrlTag);
							});
							$(curParent + " .Prd-mainImage").each(function(){
								if(curItem.mainImage.url != '') {
									$(this).attr("src",  curItem.mainImage.url);
									$(this).attr("title",curItem.mainImage.title);
									$(this).attr("alt",  curItem.mainImage.alt);
								} else {
									clearOuterHTML($(this).attr("class") || $(this).attr("className"));
								}
							});
							$(curParent + " .Prd-largeListImage").each(function(){
								if(curItem.largeListImage.url != '') {
									$(this).attr("src",  curItem.largeListImage.url);
									$(this).attr("title",curItem.largeListImage.title);
									$(this).attr("alt",  curItem.largeListImage.alt);
								} else {
									clearOuterHTML($(this).attr("class") || $(this).attr("className"));
								}
								});
							$(curParent + " .Prd-normalListImage").each(function(){
								if(curItem.normalListImage.url != '') {
									$(this).attr("src",  curItem.normalListImage.url);
									$(this).attr("title",curItem.normalListImage.title);
									$(this).attr("alt",  curItem.normalListImage.alt);
								} else {
									clearOuterHTML($(this).attr("class") || $(this).attr("className"));
								}
							});
							$(curParent + " .Prd-cartImageTag").each(function(){
								if(curItem.cartImageTag != '') {
										$(this).html(curItem.cartImageTag);
								} else {
									$(this).html('');
								}
							});
						}
					});

					$(curParent + "-productName").each(function(){
						$(this).html(curItem.productName);
					});
					$(curParent + "-brandName").each(function(){
						$(this).html(curItem.brandName);
					});
					$(curParent + "-productSummary").each(function(){
						$(this).html(curItem.productSummary);
					});
					$(curParent + "-scheduledMessage").each(function(){
						$(this).html(curItem.scheduledMessage);
					});
					$(curParent + "-productUrl").each(function(){
						$(this).html(curProductUrl);
					});
					$(curParent + "-productUrlRef").each(function(){
						$(this).attr("href", curProductUrlRef);
					});
					$(curParent + "-salesPrice").each(function(){
						$(this).html(curItem.salesPrice);
					});
					$(curParent + "-fixedPrice").each(function(){
						$(this).html(curItem.fixedPrice);
					});
					$(curParent + "-discountRate").each(function(){
						$(this).html(curItem.discountRate);
					});
					$(curParent + "-point").each(function(){
						$(this).html(curItem.point);
					});
					$(curParent + "-pointRate").each(function(){
						$(this).html(curItem.pointRate);
					});
					$(curParent + "-salesReleaseDate").each(function(){
						$(this).html(curItem.salesReleaseDate);
					});
					$(curParent + "-productNotesTitle").each(function(){
						$(this).html(curItem.productNotesTitle);
					});
					$(curParent + "-productNotesText").each(function(){
						$(this).html(curItem.productNotesText);
					});
					$(curParent + "-productNotesUrl").each(function(){
						$(this).html(curItem.productNotesUrl);
					});
					$(curParent + "-productNotesUrlTag").each(function(){
						$(this).html(curItem.productNotesUrlTag);
					});
					$(curParent + "-mainImage").each(function(){
						if(curItem.mainImage.url != '') {
							$(this).attr("src",  curItem.mainImage.url);
							$(this).attr("title",curItem.mainImage.title);
							$(this).attr("alt",  curItem.mainImage.alt);
						} else {
							clearOuterHTML($(this).attr("class") || $(this).attr("className"));
						}
					});
					$(curParent + "-largeListImage").each(function(){
						if(curItem.largeListImage.url != '') {
							$(this).attr("src",  curItem.largeListImage.url);
							$(this).attr("title",curItem.largeListImage.title);
							$(this).attr("alt",  curItem.largeListImage.alt);
						} else {
							clearOuterHTML($(this).attr("class") || $(this).attr("className"));
						}
					});
					$(curParent + "-normalListImage").each(function(){
						if(curItem.normalListImage.url != '') {
							$(this).attr("src",  curItem.normalListImage.url);
							$(this).attr("title",curItem.normalListImage.title);
							$(this).attr("alt",  curItem.normalListImage.alt);
						} else {
							clearOuterHTML($(this).attr("class") || $(this).attr("className"));
						}
					});
					$(curParent + "-cartImageTag").each(function(){
						if(curItem.cartImageTag != '') {
							$(this).html(curItem.cartImageTag);
						} else {
							if($(".Prd-Fail-cartImageTag")) {
								$(this).html($(".Prd-Fail-cartImageTag").html());
							} else {
								$(this).html('');
							}
						}
					});
				}
				if (typeof chirashiDataMake == "function") {
					chirashiDataMake();
				}
			},
			"error": function(d,msg) {
				for (var i = 0; i < skuAry.length; i++) {
					var sku = skuAry[i];
					$(".Prd-" + sku).each(function(){
						$(this).html('<div class="pubEnd"></div>');
					});
					$(".Prd-" + sku + "-productName").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-brandName").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productSummary").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-scheduledMessage").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productUrl").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productUrlRef").each(function(){
						$(this).attr('');
					});
					$(".Prd-" + sku + "-salesPrice").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-fixedPrice").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-discountRate").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-point").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-pointRate").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-salesReleaseDate").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productNotesTitle").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productNotesText").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productNotesUrl").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-productNotesUrlTag").each(function(){
						$(this).html('');
					});
					$(".Prd-" + sku + "-mainImage").each(function(){
						clearOuterHTML($(this).attr("class") || $(this).attr("className"));
					});
					$(".Prd-" + sku + "-largeListImage").each(function(){
						clearOuterHTML($(this).attr("class") || $(this).attr("className"));
					});
					$(".Prd-" + sku + "-normalListImage").each(function(){
						clearOuterHTML($(this).attr("class") || $(this).attr("className"));
					});
					$(".Prd-" + sku + "-cartImageTag").each(function(){
						$(this).html('');
					});
				}
			}
		});
	}
});

function clearOuterHTML(className){
	var elems = $("." + className).get();
	for (var i = 0; i < elems.length; i++) {
		if(elems[i].outerHTML) {
			elems[i].outerHTML = '';
		} else {
			//Firefox
			var divElement = document.createElement("div");
			divElement.appendChild(elems[i]);
			divElement.innerHTML = '';
		}
	}
}
