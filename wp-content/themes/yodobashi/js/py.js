jQuery('.dropdown-toggle').dropdown();

jQuery(function(){
	
	/* list maker filter */
	jQuery('.maker-filter>li>a').on('click', function(){
		var key = this.href.split('#');
		if ( key[1] == 'all' ) {
			jQuery('tr.lensitem').show();
		} else {
			jQuery('tr.lensitem').hide();
			jQuery('tr.'+key[1]).show();
		}
	});
	
})

jQuery(document).ready(function() {
  jQuery('.drawer').drawer();
});

