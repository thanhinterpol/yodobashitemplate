<?php
//Template Name: Home page

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">


            <div id="grid-content">
                <div>
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri() ?>/images/logo.gif" alt="" width="225" height="197">
                    </a>
                    <ul class="sidemenu">
                        <li><img src="<?php echo get_template_directory_uri() ?>/images/sr_off.gif" alt="" width="225" height="33"></li>
                        <?php
                        $categories = get_terms( 'category', array(
                            'orderby'    => 'count',
                            'hide_empty' => 0,
                        ) );
                        foreach ($categories as $cat){
                            if($cat->term_id != 1){
                                ?>
                                <li>
                                    <a href="<?php echo get_term_link($cat) ?>"><?php echo $cat->name; ?></a>
                                </li>
                                <?php
                            }
                        }
                        ?>
                        <li class="live"><a href="#"></a></li>
                        <li class="media"><a href="#"></a></li>
                        <li><img src="<?php echo get_template_directory_uri() ?>/images/bn.gif"></li>
                        <li>
                            <a href="#">2018</a>
                        </li>
                        <li>
                            <a href="#">2017</a>
                        </li>
                        <li>
                            <a href="#">2016</a>
                        </li>
                        <li>
                            <a href="#">2015</a>
                        </li>
                        <li>
                            <a href="#">2014</a>
                        </li>
                        <li>
                            <a href="#">2013</a>
                        </li>
                        <li>
                            <a href="#">2012</a>
                        </li>
                        <li>
                            <a href="#">2011</a>
                        </li>
                        <li>
                            <a href="#">2010</a>
                        </li>
                        <li>
                            <a href="#">2009</a>
                        </li>
                    </ul>
                </div>


                <!--***-->
                <?php
                $args = array(
                    'posts_per_page'    => -1,
                    'post_type' => 'post'
                );
                $the_query = new WP_Query( $args ); ?>

                <?php if ( $the_query->have_posts() ) : ?>
                    <?php while ( $the_query->have_posts() ) : $the_query->the_post(); ?>
                        <div class="" style="position: absolute; left: 555px; top: 0px;">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_post_thumbnail('full'); ?>
                            </a>
                            <img src="<?php echo get_template_directory_uri() ?>/images/lensnew.gif" alt="" width="35" height="14" class="itsnew">
                        </div>
                    <?php endwhile; ?>
                    <?php wp_reset_postdata(); ?>

                <?php else : ?>
                <?php endif; ?>
            </div>

		</main><!-- #main -->
	</div><!-- #primary -->
    <script type="text/javascript">
        //<![CDATA[
        jQuery(function () {
            jQuery("#grid-content").vgrid({
            });
        });

    </script>
<?php
get_footer();
