<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package dichvuweb
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<header id="masthead" class="site-header">
        <?php if(is_home() || is_front_page()){

        }else{
            ?>
        <header role="banner" id="sidenavi">
            <button type="button" class="drawer-toggle drawer-hamburger">
                <span class="sr-only">toggle navigation</span>
                <span class="drawer-hamburger-icon"></span>
            </button>
            <nav class="drawer-nav" role="navigation" style="touch-action: none;">

                <?php
                wp_nav_menu(
                    array(
                        'theme_location' => 'menu-1',
                        'menu_class' => 'drawer-menu',
                        'walker'  => new Walker_Quickstart_Menu()
                    )
                );
                ?>
            </nav>
        </header>

        <div class="container" role="main">

            <div class="row">
                <header class="col-md-6 col-sm-5">
                    <h1><a href="<?php echo esc_url( home_url( '/' ) ); ?>">PHOTO YODOBASHI</a></h1>
                    <p class="sub-title">ヨドバシカメラ公式オンライン写真マガジン</p>
                </header>

                <nav class="main-nav col-md-6 col-sm-7">
                    <?php
                    wp_nav_menu(
                        array(
                            'theme_location' => 'menu-1',
                            'menu_class' => 'nav nav-pills navbar-right',
                            'walker'  => new Walker_Quickstart_Menu_dk()
                        )
                    );
                    ?>
                </nav>

                <div class="col-xs-12">
                    <ol itemscope="" itemtype="http://schema.org/BreadcrumbList" class="breadcrumb">
                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemscope="" itemtype="http://schema.org/Thing" itemprop="item"
                               href="http://photo.yodobashi.com/">
                                <span itemprop="name">PY HOME</span>
                            </a>
                            <meta itemprop="position" content="1">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemscope="" itemtype="http://schema.org/Thing" itemprop="item"
                               href="http://photo.yodobashi.com/canon/">
                                <span itemprop="name">Canon</span>
                            </a>
                            <meta itemprop="position" content="2">
                        </li>

                        <li itemprop="itemListElement" itemscope="" itemtype="http://schema.org/ListItem">
                            <a itemscope="" itemtype="http://schema.org/Thing" itemprop="item"
                               href="http://photo.yodobashi.com/canon/lens/">
                                <span itemprop="name">Lens</span>
                            </a>
                            <meta itemprop="position" content="3">
                        </li>

                        <li>
                            <strong>Canon EF85mm F1.4L IS USM</strong>
                        </li>

                    </ol>

                </div><!-- breadcrumb -->

            </div><!-- .row : head -->
            <?php
        } ?>

	</header><!-- #masthead -->

	<div id="content" class="site-content">
