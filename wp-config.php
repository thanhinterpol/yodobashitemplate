<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'yodobashi');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'm04#6/SK;8@${O1?g1]IFnTv8w&zx.;NM:gLW095*Zlv^E)6jsY)o]%~IJ(}c;$[');
define('SECURE_AUTH_KEY',  '?r+y>+C:4{jH+ 2X1cl~ww*;HOPs&Y<I0g49FnNRv%MWu73rJIW.R0vQU?sXA{OY');
define('LOGGED_IN_KEY',    '9b2<3SSHt(qKq[T[HES7<r3ZOF7<*QV>%EE-/gbe$jfozx)nNnd0Ag[=!YLla&8N');
define('NONCE_KEY',        '5<NdgbfTaisSH4GPT3NYu5:zbV65x?8cqf&.g /!itr,>,M=iI8%<Fa!pSg3O},h');
define('AUTH_SALT',        '^Lxg8,34#aon2.h)LY_!Y:~e,2LQ^Ox3-deQm_4v|AB`:bSsS6s^!gCvEG[dG#sp');
define('SECURE_AUTH_SALT', '2Uq(gzF2Dgrl4<muYk88}mUd9m2(}E,)NEDhHOpuMe1c6*Ig$kC-Nxo- wlmqj]L');
define('LOGGED_IN_SALT',   'cJ6E.ziV%LCA:jF$`V78`DyqY%-}@^v@=qRd!&u<{W[#LuVJrE<^T{j[Wg=/Js[W');
define('NONCE_SALT',       'f~Unt]qGo&qEh%yymgshrNe,.+%KO@9B9Joa6cBj,ij1sHY#gAv6N7PP;4_9v8SC');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'yodo_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
